var targetDrag = null;
var tox, toy;
var container = $('.container');
var balls = $('.ball');
var can = $('.can');



var mDown = function (event) {

  targetDrag = $(event.target);
  tox = event.offsetX;
  toy = event.offsetY;
  container.append(targetDrag);
  targetDrag.css('background-color', 'orange');
  $(window).on('mousemove', mMove);
};
var mMove = function (event) {
  if(!targetDrag) {
    return;
  }
  targetDrag.css({left: event.pageX-tox, top:event.pageY-toy});
};
var mUp = function (event) {
  targetDrag.css('background-color', 'red');

  var canx = can.position().left + 50;
  var cany = can.position().top + 50;
  var tx = targetDrag.position().left + 30;
  var ty = targetDrag.position().top + 30;

  var dx = canx - tx;
  var dy = cany - ty;
  var distance = Math.pow(dx*dx+dy*dy, .5);

  if(distance<200) {
    targetDrag.css({
      left: canx - 30,
      top: cany -30
    });
    targetDrag.fadeOut();
  }

  targetDrag = null;
  $(window).off('mousemove');
};

balls.on('mousedown', mDown);
$(window).on('mouseup', mUp);
